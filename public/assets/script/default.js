$(function () {
    new WOW().init()
    $('.KV').slick({
        infinite: true,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
    })
    $('.Reviews').slick({
        infinite: true,
        dots: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 4000,
    })
    $('.Reviews').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var data = { event: 'command', func: 'pauseVideo', args: '' }
        var message = JSON.stringify(data)
        $('iframe', slick.$slides[currentSlide])[0].contentWindow.postMessage(message, '*')
    })
    $(window).on('scroll', function () {
        // var point = $('.point'),
        //     percentage =
        //         parseInt(
        //             ($(window).scrollTop() / ($(document).height() - $(window).height())) * 100,
        //             0,
        //         ) + '%'

        // point.css('top', percentage)
        parallaxScroll()
    })
    $(window).resize(function () {
        $('.KV').slick('resize')
        $('.Reviews').slick('resize')
    })
    $('.floatBtn').click(function () {
        // scroll to bottom of the page
        $('html, body').animate({ scrollTop: $(document).height() }, 1000)
    })
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
    })
})
/* Scroll the background layers */
function parallaxScroll() {
    var scrolled = $(window).scrollTop()
    $('.dec01').css('top', 500 - scrolled * 0.5 + 'px')
    $('.dec02').css('top', 2500 - scrolled * 0.35 + 'px')
    $('.dec03').css('top', 5500 - scrolled * 0.4 + 'px')
}
